.so ./macros
.AU
Ethan Long
.TL
Information for the halfway deadline (16/07/2021)

.AB
As of the halfway GSoC deadline,
no parts of nIME are complete,
but concrete development paths have been established.
More work will need to be done to flesh out the concepts,
but the backend design is mostly there.
.AE

.HC
How to use the current project (16/07/2021)
.PP
As of 16/07/2021, the programs in
.CW nIME
are not fully developed.
As such,
only barebones demonstrations are currently in the project.

To build the current programs,
simply run
.CW "mk all"
in the
.CW src
directory.

For a quick demonstration of all the modules being used together,
run any of the following:

.P1
echo -n 'korega!tesuto!desuyo' | to-kana.out -s | optshow.out -s
echo -n 'konnnichihasekai' | to-kana.out -s | optshow.out -s
echo -n '\f(Jpこれがテストだよ。パソコンで日本語を使います。\f(CW' | optshow.out -s
.P2

In the final build,
a full filesystem will be used to express all the intermediate steps,
no
.CW stdin ,
.CW stdout
or piping will be necessary.

.HC
Using
.CT to-kana
.PP
To use
.CW to-kana ,
run the following:

.P1
echo -n 'korega!tesuto!desuyo' | to-kana.out -s
echo -n 'konnnichihasekai' | to-kana.out -s
.P2

For more information,
see
.CW to-kana.pdf
in the docs.

.HC
Using
.CT optshow
.PP
To use
.CW optshow ,
run the following:

.P1
echo -n '\f(Jpこれがテストですよ\f(CW' | optshow.out -s
echo -n '\f(Jp今日は、世界\f(CW' | optshow.out -s
.P2

.CW optshow
should take control of the window,
and display the input
.CW Rune s
in a box at either the top or bottom of the screen.

For more information,
see
.CW optshow.pdf
in the docs.

.HC
Final Note
.PP
As a final note,
I will admit that this project is much farther behind schedule than expected.
I did not expect it to be this difficult to get the time to program,
but unforseen circumstances in life have caused it to be behind schedule.

At the current time,
I have full confidence that I will be able to complete
.CW nIME
before the final deadline.
