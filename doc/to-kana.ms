.so ./macros
.AU
Ethan Long
.TL
.CT to-kana,
a program for converting romaji to kana.

.AB
.CW to-kana
converts a incoming stream of romaji to either hiragana or katakana either from a file,
or from standard input, and outputs to a file or standard output.
.CW to-kana
aims to be as modular as possible,
it should be usable for any project that requires a stream of romaji to be converted.
.AE

.HC
Purpose in
.CT nIME
.PP
.CW nIME
uses
.CW to-kana
to convert the stream of keyboard inputs into basic kana for
.CW to-kanji
to then convert to kanji.
.CW to-kana
plays a very pivotal role in
.CW nIME ,
without it no kana conversion could take place.

.HC
Usage
.PP
.CW to-kana
takes in a very rigid but simple form of romaji,
every character except the basic vowels takes two letters to type.
The style is most similar to the ``modern'' IME,
rather than
.CW ktrans '
SKK format.
Some examples of common usage:

.ce 4
\f(CWwatashi\fR → \f(Jpわたし\fR
\f(CWkorega!epikku!desu\fR → \f(Jpこれがエピックです\fR
\f(CWnannde\fR → \f(Jpなんで\fR
\f(CWdonnna\fR → \f(Jpどんな\fR

In the raw stream,
.CW ! ' `
is used as a placeholder for the katakana trigger character,
.CW nIME 's
GUI will trigger said placeholder when conversion is requested.

As one can see from this usage,
the common letters all take two keystrokes to type.

.TS
c|c
= =
c|c
c|c
c|c
c|c
c|c
c|c.
\f(Jp日本語\fR	\f(Jpロマ字\fR

\f(Jpあ\fR	a
\f(Jpか\fR	ka
\f(Jpきゃ\fR	kya
\f(Jpっか\fR	kka
\f(Jpっきゃ\fR	kkya
\f(Jpん\fR	nn
.TE


.HC
Function documentation
.PP
All the functions in
.CW to-kana
serve the purposes of generating kana from a stream.
.SC
The
.CI eval
function
.PP
The 
.CW eval
function takes in a file pointer and converts the contents assuming that they are in the previously described romaji form.
The function should run with buffered IO (as of writing buffered IO has not been implemented yet),
waiting on any new input from a file,
and outputting to a separate file.
The final filesystem layout is TBD.

.SC
The
.CI strappend
function
.PP
The
.CW strappend
function takes in a pointer to a string,
along with a character to append,
it then appends the character in the first null address of the string.

Additionally,
since its purpose is in the context of Japanese input,
it returns an integer value representing whether or not the string now represents a full Japanese character in Romaji,
if it does it returns 1.

.SC
The
.CI kanalook
function
.PP
The 
.CW kanalook
function takes in a pointer to a complete Romaji buffer representing a single syllable,
and converts the output to a string of
.CW Rune s.
It does this with the help of the
.CW kanafill
function.

.SC
The
.CI kanafill
function
.PP
The
.CW kanafill
function is designed to output a string of runes representing a single syllable in kana.
It takes in a
.CW Rune
representing the base 'a' family of the kana
(like \f(Jpあ\fR, \f(Jpか\fR, \f(Jpら\fR, etc.),
and then uses the
.CW in
string of characters representing the ending of the kana,
along with an integer representing the presence of dakuten to determine which kana / kana combination to output.

Here is an example table of output from the
.CW kanafill
function:

.TS
 - - - -
|c|c|c|c|
 = = = =
|c|c|c|c|
|c|c|c|c|
|c|c|c|c|
|c|c|c|c|
|c|c|c|c|
|c|c|c|c|
|c|c|c|c|
 - - - -.

T{
.CW "Rune base"
T}	T{
.CW "char* in"
T}	T{
.CW "int dakuten"
T}	T{
.CW "Rune* out"
T}

\f(Jpか\fR	a	0	\f(Jpか\fR
\f(Jpか\fR	e	0	\f(Jpけ\fR
\f(Jpか\fR	i	1	\f(Jpぎ\fR
\f(Jpは\fR	i	2	\f(Jpぴ\fR
\f(Jpは\fR	ya	0	\f(Jpひゃ\fR
\f(Jpは\fR	yu	2	\f(Jpぴゅ\fR
\f(Jpハ\fR	u	0	\f(Jpフ\fR

.TE

