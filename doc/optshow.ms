.so ./macros
.AU
Ethan Long
.TL
.CT optshow,
a program for showing possible convertion options from an IME.

.AB
.CW optshow
is a program designed to interact with an underlying text based IME,
optshow flicks through the possible convertion options and outputs the options as a GUI menu.
.CW optshow
should work with any IME with similar interaction to
.CW ktrans ,
I.E. the IME requires a key to be hit to cycle through possible convertions.
.AE

.HC
Purpose in
.CT nIME
.PP
.CW nIME
will use
.CW optshow
for its intended purpose,
to show the potential kanji convertions available for output.

.HC
Usage
.PP
.CW optshow
in its final incantation will cycle through conversions with a given IME,
and output a menu with a list of options to select.

As of the time of writing, 
.CW optshow
is simply a proof of concept and so it simply shows whatever is input into it if the
.CW -s
flag is given.

.CW optshow
should position itself near the cursor or in a convenient position to read while typing,
fonts should be able to be as big as desired.

.HC
Function documentation
.PP
.CW optshow
is a threaded program,
and so the functions mostly run concurrently sending messages through channels.

.SC
The
.CI clockproc
function
.PP
The
.CW clockproc
function is made to be run as a separate process.
It runs as a clock,
sending a pulse over the provided channel every second.
The channel should be an integer,
clockproc will set the channel to 1 every pulse.
If clockproc has send a 0,
then something has broken with the sending of a pulse and the process will crash.

This can be useful for any routine checks,
like refreshing the position (although this should also be triggered by any movement anyway),
or for syncing up channel communication.
The final version of
.CW optshow
likely won't use this process.

.SC
The
.CI optdraw
function
.PP
.CW optdraw will take in a array of Rune strings,
and display them as a menu for the user to interact with either through the keyboard or mouse.
The final incantation of
.CW optdraw
will output an integer corresponding to the selected option.

.SC
The
.CI readinproc
function
.PP
The
.CW readinproc
function is designed to be ran as a process.
.CW readinproc
should return a string of Runes as input to the
.CW INPUT
member of the interaction alts,
which can then be sent to optdraw to be evaluated.

.SC
The
.CI relocate
function
.PP
When run,
the
.CW relocate
function should relocate the menu to where it needs to be,
a place near the typing that is convenient to read for the user typing.
The current method of imlementation is through wctl.
