#include <u.h>
#include <stdio.h>
#include <libc.h>
#include <String.h>

void
test(char in[2], int dakuten, Rune* out)
{
	Rune letter = (Rune)((int)(L'か') + dakuten);
	Rune str[] = {letter};
	//print("%S, %C", str, letter);
	out[0] = letter;
	return;
}

Rune*
withinfunc(void)
{
	Rune str[3];
	test("aa", 1, str);
	return(str);
}

void
main()
{
	Rune str[3];
	test("aa", 1, str);
	print("%S", str);
	
	Rune* string2 = withinfunc();
	print("%S\n", str);
	
	print("%d , %d\n", L'あ' , L'か');
	print("%d , %d\n", L'か' , L'さ');
	print("%d , %d\n", L'さ' , L'た');
	print("%d , %d\n", L'た' , L'な');
	print("%d , %d\n", L'な' , L'は');
	print("%d , %d\n", L'は' , L'ま');
	print("%d , %d\n", L'ま' , L'や');
	print("%d , %d\n", L'や' , L'ら');
	print("%d , %d\n", L'ら' , L'わ');
	for(int i = 0; i < 100; i++){
		print("%C\n", (Rune)(12353 + i));
	}
	
	print("%d , %d\n", (1 == 1), (1 != 1));
	if(1){
		print("True\n");
	}
	
	char teststring[3] = "abc";
	print("%s\n", teststring);
	teststring[0] = 0;
	print("%s\n", teststring);
	char lol[3] = "lol";
	Rune tesuto[4] = L"テスト";
	int fp = fileno(stdout);
	fprint(fp, "%s\n%S\n", lol, tesuto);
	
	print("%d , %d\n", L'あ', L'ア');
	print("%d , %d\n", L'か', L'カ');
	for(int i = 0; i < 100; i++){
		print("%C , %C\n", (Rune)(12353 + i), (Rune)(12353 + 96 + i));
	}
	exits(nil);
}
