/*
 *******************************************************************************
 * Author: Ethan Long
 * Licence: Public Domain
 * Email: ethandavidlong@gmail.com, u7281759@anu.edu.au
 * Description: fserv will take an IME's output and present it to opshow to
 *              show the available conversion options to the user.
 */

#include <u.h>
#include <libc.h>
#include <thread.h>
#include <bio.h>


